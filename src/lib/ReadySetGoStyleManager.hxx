/* -*- Mode: C++; c-default-style: "k&r"; indent-tabs-mode: nil; tab-width: 2; c-basic-offset: 2 -*- */

/* libmwaw
* Version: MPL 2.0 / LGPLv2+
*
* The contents of this file are subject to the Mozilla Public License Version
* 2.0 (the "License"); you may not use this file except in compliance with
* the License or as specified alternatively below. You may obtain a copy of
* the License at http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
* for the specific language governing rights and limitations under the
* License.
*
* Major Contributor(s):
* Copyright (C) 2002 William Lachance (wrlach@gmail.com)
* Copyright (C) 2002,2004 Marc Maurer (uwog@uwog.net)
* Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
* Copyright (C) 2006, 2007 Andrew Ziem
* Copyright (C) 2011, 2012 Alonso Laurent (alonso@loria.fr)
*
*
* All Rights Reserved.
*
* For minor contributions see the git repository.
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU Lesser General Public License Version 2 or later (the "LGPLv2+"),
* in which case the provisions of the LGPLv2+ are applicable
* instead of those above.
*/

#ifndef READYSETGO_STYLE_MANAGER
#  define READYSETGO_STYLE_MANAGER

#include <string>
#include <vector>

#include <librevenge/librevenge.h>

#include "MWAWDebug.hxx"
#include "MWAWGraphicStyle.hxx"
#include "MWAWInputStream.hxx"

#include "MWAWParser.hxx"

struct MWAWTabStop;

namespace ReadySetGoStyleManagerInternal
{
struct State;
}

class ReadySetGoParser;

/** \brief class to read/store ReadySetGo v1-v6 styles
 *
 */
class ReadySetGoStyleManager
{
  friend class ReadySetGoParser;
public:
  //! constructor
  explicit ReadySetGoStyleManager(ReadySetGoParser &parser);
  //! destructor
  virtual ~ReadySetGoStyleManager();

  //! try to retrieve the i^th color
  bool getColor(int colorId, MWAWColor &color) const;
  //! try to retrieve a pattern
  bool getPattern(int patternId, MWAWGraphicStyle::Pattern &pattern) const;

  //! try to read a style: v3
  bool readStyle(MWAWFont &font, MWAWParagraph &para, int *cPos=nullptr);
  //! try to read a list of tabulations: v1-2
  bool readTabulationsV1(std::vector<MWAWTabStop> &tabulations, std::string &extra);
  //! try to read a list of tabulations: v3
  bool readTabulations(std::vector<MWAWTabStop> &tabs, long len=-1, int *cPos=nullptr);

protected:
  //! returns the file version
  int version() const;

  //
  // data
  //

  //! try to read the list of colors: v4.5
  bool readColors();
  //! try to read the list of colors: v4.5
  bool readColorNames();
  //! try to read the list of font block: unsure, name + data?, v4.5
  bool readFontsBlock();
  //! try to read the optional list of font block: name + data?, v6
  bool readFontsBlock2();
  //! try to read the list of font name: v6
  bool readFontsName();

  //! try to read the list of style block: v4
  bool readStyles(int numStyles);
protected:
  //! the main parser
  ReadySetGoParser &m_parser;
  //! the parser state
  MWAWParserStatePtr m_parserState;
  //! the state
  std::shared_ptr<ReadySetGoStyleManagerInternal::State> m_state;
};
#endif
// vim: set filetype=cpp tabstop=2 shiftwidth=2 cindent autoindent smartindent noexpandtab:
