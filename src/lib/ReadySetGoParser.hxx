/* -*- Mode: C++; c-default-style: "k&r"; indent-tabs-mode: nil; tab-width: 2; c-basic-offset: 2 -*- */

/* libmwaw
* Version: MPL 2.0 / LGPLv2+
*
* The contents of this file are subject to the Mozilla Public License Version
* 2.0 (the "License"); you may not use this file except in compliance with
* the License or as specified alternatively below. You may obtain a copy of
* the License at http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
* for the specific language governing rights and limitations under the
* License.
*
* Major Contributor(s):
* Copyright (C) 2002 William Lachance (wrlach@gmail.com)
* Copyright (C) 2002,2004 Marc Maurer (uwog@uwog.net)
* Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
* Copyright (C) 2006, 2007 Andrew Ziem
* Copyright (C) 2011, 2012 Alonso Laurent (alonso@loria.fr)
*
*
* All Rights Reserved.
*
* For minor contributions see the git repository.
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU Lesser General Public License Version 2 or later (the "LGPLv2+"),
* in which case the provisions of the LGPLv2+ are applicable
* instead of those above.
*/

#ifndef READYSETGO_PARSER
#  define READYSETGO_PARSER

#include <string>
#include <vector>

#include <librevenge/librevenge.h>

#include "MWAWDebug.hxx"
#include "MWAWInputStream.hxx"

#include "MWAWParser.hxx"

namespace ReadySetGoParserInternal
{
struct State;
}

class ReadySetGoGraph;
class ReadySetGoStyleManager;

/** \brief the main class to read a ReadySetGo 1.0,2.1,3.0,4.0,4.5,6.0 file
 *
 * \note as I found a bunch of 7.7 demo files, I try to make the parser to accept them
 * without garanty
 */
class ReadySetGoParser final : public MWAWGraphicParser
{
  friend class ReadySetGoGraph;
  friend class ReadySetGoStyleManager;
public:
  //! constructor
  ReadySetGoParser(MWAWInputStreamPtr const &input, MWAWRSRCParserPtr const &rsrcParser, MWAWHeader *header);
  //! destructor
  ~ReadySetGoParser() final;

  //! checks if the document header is correct (or not)
  bool checkHeader(MWAWHeader *header, bool strict=false) final;

  //! the main parse function
  void parse(librevenge::RVNGDrawingInterface *documentInterface) final;

  // interface with sub parser

  /** returns true if the file is a design studio 2 file

      \note in this case, version() will return 6
   */
  bool isDesignStudioFile() const;

protected:
  //! creates the listener which will be associated to the document
  void createDocument(librevenge::RVNGDrawingInterface *documentInterface);

protected:
  //! finds the different objects zones
  bool createZones();
  //! try to read the document header: v3
  bool readDocument();
  //! try to read an unknown list of IDs: v3
  bool readIdsList();
  //! try to read the glossary text list: v4
  bool readGlossary();
  //! try to read the glossary object list: v6
  bool readObjectGlossary();
  //! try to read an unknown font zone: v6
  bool readFontsUnknown();
  //! try to read an unknown zone: v6
  bool readZone11();

  // Intermediate level

  //! try to read the print info zone
  bool readPrintInfo();

  //
  // low level
  //

  //
  // data
  //
  //! the graph parser
  std::shared_ptr<ReadySetGoGraph> m_graphParser;
  //! the style manager
  std::shared_ptr<ReadySetGoStyleManager> m_styleManager;
  //! the state
  std::shared_ptr<ReadySetGoParserInternal::State> m_state;
};
#endif
// vim: set filetype=cpp tabstop=2 shiftwidth=2 cindent autoindent smartindent noexpandtab:
