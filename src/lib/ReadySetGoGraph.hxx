/* -*- Mode: C++; c-default-style: "k&r"; indent-tabs-mode: nil; tab-width: 2; c-basic-offset: 2 -*- */

/* libmwaw
* Version: MPL 2.0 / LGPLv2+
*
* The contents of this file are subject to the Mozilla Public License Version
* 2.0 (the "License"); you may not use this file except in compliance with
* the License or as specified alternatively below. You may obtain a copy of
* the License at http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
* for the specific language governing rights and limitations under the
* License.
*
* Major Contributor(s):
* Copyright (C) 2002 William Lachance (wrlach@gmail.com)
* Copyright (C) 2002,2004 Marc Maurer (uwog@uwog.net)
* Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
* Copyright (C) 2006, 2007 Andrew Ziem
* Copyright (C) 2011, 2012 Alonso Laurent (alonso@loria.fr)
*
*
* All Rights Reserved.
*
* For minor contributions see the git repository.
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU Lesser General Public License Version 2 or later (the "LGPLv2+"),
* in which case the provisions of the LGPLv2+ are applicable
* instead of those above.
*/

/*
 * Parser to ReadySetGo 1-6 document ( graphic part )
 *
 */
#ifndef READYSETGO_GRAPH
#  define READYSETGO_GRAPH

#include <string>
#include <vector>

#include <librevenge/librevenge.h>

#include "libmwaw_internal.hxx"

#include "MWAWDebug.hxx"
#include "MWAWInputStream.hxx"

namespace ReadySetGoGraphInternal
{
struct Layout;
struct Shape;
struct State;
class SubDocument;
}

class ReadySetGoParser;
class ReadySetGoStyleManager;

/** \brief the main class to read the graphic part of a ReadySetGo 1-6 file
 *
 *
 *
 */
class ReadySetGoGraph
{
  friend class ReadySetGoParser;
  friend class ReadySetGoGraphInternal::SubDocument;

public:
  //! constructor
  explicit ReadySetGoGraph(ReadySetGoParser &parser);
  //! destructor
  virtual ~ReadySetGoGraph();

protected:

  /** returns the file version */
  int version() const;

  //
  // Intermediate level
  //

  //! try to read a list of shape
  bool readShapes();
  //! try to read a list of shape corresponding to an object glossary: need in v6
  bool readShapesInObject();

  //! try to update the textboxes link: v3
  bool updateTextBoxLinks();
  /** update the page span list */
  void updatePageSpanList(std::vector<MWAWPageSpan> &spanList) const;

  //! try to send the master pages
  bool sendMasterPages();
  //! try to send each main pages
  bool sendPages();
  //! try to send the shapes of a layout
  bool send(ReadySetGoGraphInternal::Layout const &layout);

  //! try to send a shape
  bool send(ReadySetGoGraphInternal::Shape const &shape);
  //! try to send the text corresponding to a shape
  bool sendText(ReadySetGoGraphInternal::Shape const &shape);

  //
  // low level
  //

  //! try to read the layout list: v3
  bool readLayoutsList(int numLayouts, bool master=false);
  //! try to read a shape: v1
  bool readShapeV1();
  //! try to read a shape: v2
  bool readShapeV2(ReadySetGoGraphInternal::Layout &layout);
  //! try to read a shape: v3
  bool readShapeV3(ReadySetGoGraphInternal::Layout &layout, bool &last);
  //! try to read a shape: v6
  bool readShapeV6(ReadySetGoGraphInternal::Layout &layout, bool &last);
  //! try to read a shape: design studio v2
  bool readShapeDSV2(ReadySetGoGraphInternal::Layout &layout, bool &last);

private:
  ReadySetGoGraph(ReadySetGoGraph const &orig) = delete;
  ReadySetGoGraph &operator=(ReadySetGoGraph const &orig) = delete;

protected:
  //
  // data
  //

  //! the state
  std::shared_ptr<ReadySetGoGraphInternal::State> m_state;

  //! the main parser;
  ReadySetGoParser &m_parser;
  //! the style manager
  std::shared_ptr<ReadySetGoStyleManager> m_styleManager;

};
#endif
// vim: set filetype=cpp tabstop=2 shiftwidth=2 cindent autoindent smartindent noexpandtab:
