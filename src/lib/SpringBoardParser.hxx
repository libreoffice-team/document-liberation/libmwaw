/* -*- Mode: C++; c-default-style: "k&r"; indent-tabs-mode: nil; tab-width: 2; c-basic-offset: 2 -*- */

/* libmwaw
* Version: MPL 2.0 / LGPLv2+
*
* The contents of this file are subject to the Mozilla Public License Version
* 2.0 (the "License"); you may not use this file except in compliance with
* the License or as specified alternatively below. You may obtain a copy of
* the License at http://www.mozilla.org/MPL/
*
* Software distributed under the License is distributed on an "AS IS" basis,
* WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
* for the specific language governing rights and limitations under the
* License.
*
* Major Contributor(s):
* Copyright (C) 2002 William Lachance (wrlach@gmail.com)
* Copyright (C) 2002,2004 Marc Maurer (uwog@uwog.net)
* Copyright (C) 2004-2006 Fridrich Strba (fridrich.strba@bluewin.ch)
* Copyright (C) 2006, 2007 Andrew Ziem
* Copyright (C) 2011, 2012 Alonso Laurent (alonso@loria.fr)
*
*
* All Rights Reserved.
*
* For minor contributions see the git repository.
*
* Alternatively, the contents of this file may be used under the terms of
* the GNU Lesser General Public License Version 2 or later (the "LGPLv2+"),
* in which case the provisions of the LGPLv2+ are applicable
* instead of those above.
*/

#ifndef SPRINGBOARD_PARSER
#  define SPRINGBOARD_PARSER

#include <string>
#include <utility>
#include <vector>

#include <librevenge/librevenge.h>

#include "MWAWDebug.hxx"
#include "MWAWInputStream.hxx"

#include "MWAWParser.hxx"

namespace SpringBoardParserInternal
{
struct State;

class SubDocument;
}

/** \brief the main class to read a SpringBoard v1 file
 *
 */
class SpringBoardParser final : public MWAWGraphicParser
{
  friend class SpringBoardParserInternal::SubDocument;
public:
  //! constructor
  SpringBoardParser(MWAWInputStreamPtr const &input, MWAWRSRCParserPtr const &rsrcParser, MWAWHeader *header);
  //! destructor
  ~SpringBoardParser() final;

  //! checks if the document header is correct (or not)
  bool checkHeader(MWAWHeader *header, bool strict=false) final;

  // the main parse function
  void parse(librevenge::RVNGDrawingInterface *documentInterface) final;

protected:
  //! creates the listener which will be associated to the document
  void createDocument(librevenge::RVNGDrawingInterface *documentInterface);
  //! try to parse the page numbering zone
  bool parsePageNumbering(MWAWEntry const &entry);
  //! try to parse a list of data
  bool parseDataList(int id, MWAWEntry const &entry);
  //! try to parse a second entry zones
  bool parseLastZone(int id, MWAWEntry const &entry);

  //! try to parse a list of frames corresponding to a page
  bool parseFrames(int page, long pos);

  // data

  //! try to parse some data
  bool parseData(int id, MWAWEntry const &entry, MWAWVec2i const &indices);
  //! try to parse a list of paragraph style: zone8
  bool parseParagraphStyle(MWAWEntry const &entry, MWAWVec2i const &indices);
  //! try to parse a shape
  bool parseShape(long pos, std::pair<int,int> const &pageId);

protected:

  //! finds the different objects zones
  bool createZones();

  // Intermediate level

  //! try to read a font
  bool readFont(MWAWFont &font, int szFieldSize);

  //! try to send a header/footer text
  bool sendHeaderFooter(int hfId);
  //! try to send a frame (with a shape)
  bool sendFrame(std::pair<int,int> const &pageId);
  //! try to send a shape text
  bool sendTextShape(std::pair<int,int> const &pageId, int minParagraph=0, int maxParagraph=-1);
  //! try to send a link text
  bool sendLinkShape(std::pair<int,int> const &linkId, bool isTop);

  //
  // low level
  //

  /** returns true if the position is a valid file positions,
      ie. exists in the file and not in a already parsed zone */
  bool isPositionValid(long pos) const;
  /** returns true if the zone is a valid zone positions,
      ie. exists in the file and not in a already parsed zone */
  bool isEntryValid(MWAWEntry const &entry) const;

  //
  // data
  //

  //! the state
  std::shared_ptr<SpringBoardParserInternal::State> m_state;
};
#endif
// vim: set filetype=cpp tabstop=2 shiftwidth=2 cindent autoindent smartindent noexpandtab:
